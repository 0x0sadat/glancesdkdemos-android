package net.glance.twowayvideodemo_java;

import static net.glance.twowayvideodemo_java.Constants.GLANCE_GROUP_ID;
import static net.glance.twowayvideodemo_java.Constants.VIDEO_MODE;
import static net.glance.twowayvideodemo_java.Constants.VISITOR_ID;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabColorSchemeParams;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;

import net.glance.android.Event;
import net.glance.android.EventCode;
import net.glance.android.HostSession;
import net.glance.android.PresenceVisitor;
import net.glance.android.StartParams;
import net.glance.android.Visitor;
import net.glance.android.VisitorInitParams;
import net.glance.android.VisitorListener;
import net.glance.twowayvideodemo_java.databinding.FragmentFirstBinding;

public class FirstFragment extends Fragment implements VisitorListener {

    private FragmentFirstBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = FragmentFirstBinding.inflate(inflater, container, false);

        Visitor.addMaskedView(binding.textviewSecond);

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FirstFragment self = this;

        binding.buttonStart.setOnClickListener(view1 -> {
            Visitor.addListener(self);

            StartParams startParams = new StartParams();
            startParams.setKey("GLANCE_KEYTYPE_RANDOM");
            startParams.setVideo(VIDEO_MODE);
            Visitor.startSession(startParams);
        });

        binding.buttonPresence.setOnClickListener(v -> {
            if (PresenceVisitor.isConnected()) {
                PresenceVisitor.disconnect();
            } else {
                VisitorInitParams initParams = new VisitorInitParams(GLANCE_GROUP_ID);
                initParams.setVisitorId(VISITOR_ID);

                Visitor.init(getActivity(), initParams);
                Visitor.addListener(self);

                PresenceVisitor.connect();
            }
        });

        binding.buttonCustomTabs.setOnClickListener(v -> {
            String url = "https://google.com/";
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder()
                    .setInitialActivityHeightPx(40)
                    .setCloseButtonPosition(CustomTabsIntent.CLOSE_BUTTON_POSITION_END);

            CustomTabColorSchemeParams defaultColors = new CustomTabColorSchemeParams.Builder()
                    .setToolbarColor(self.getResources().getColor(R.color.teal_700))
                    .build();
            builder.setDefaultColorSchemeParams(defaultColors);

            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(self.getContext(), Uri.parse(url));
        });

        binding.buttonWebview.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), WebViewActivity.class);
            i.putExtra("url", "https://d2e93a2oavc15x.cloudfront.net/");
            i.putExtra("querySelectors", ".mask_1, .mask_2, #mask_3, .mask_4, span, #hplogo");
            i.putExtra("labels", "mask 1, mask 2, mask 3, mask 4, span, LOGO");
            startActivity(i);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Visitor.removeListener(this);
        binding = null;
    }

    @Override
    public void onGlanceVisitorEvent(Event event) {
        if (event.getCode() == EventCode.EventVisitorInitialized) {
            Log.d("GLANCE", "EventVisitorInitialized");
        } else if (event.getCode() == EventCode.EventConnectedToSession) {
            getActivity().runOnUiThread(() -> {
                if (binding != null) {
                    binding.buttonStart.setVisibility(View.GONE);
                    binding.buttonPresence.setVisibility(View.GONE);
                    binding.buttonWebview.setVisibility(View.VISIBLE);
                    binding.buttonCustomTabs.setVisibility(View.VISIBLE);

                    if (PresenceVisitor.isConnected()) {
                        anchorViews(binding.clFirstFragmentContainer, binding.buttonPresence, ConstraintSet.TOP,
                                binding.buttonCustomTabs, ConstraintSet.BOTTOM, R.dimen.enabled_presence_bt_margin_top);
                    }
                }
            });
        } else if (event.getCode() == EventCode.EventSessionEnded) {
            getActivity().runOnUiThread(() -> {
                if (binding != null) {
                    binding.buttonStart.setVisibility(View.VISIBLE);
                    binding.buttonPresence.setVisibility(View.VISIBLE);
                    binding.buttonWebview.setVisibility(View.GONE);
                    binding.buttonCustomTabs.setVisibility(View.GONE);

                    if (PresenceVisitor.isConnected()) {
                        anchorViews(binding.clFirstFragmentContainer, binding.buttonPresence, ConstraintSet.TOP,
                                binding.buttonStart, ConstraintSet.BOTTOM, R.dimen.default_presence_bt_margin_top);
                    }
                }
            });
        }

        // PRESENCE CODES
        else if (event.getCode() == EventCode.EventPresenceConnected) {
            getActivity().runOnUiThread(() -> {
                binding.textviewFirst.setText(String.format("Visitor id: %s", VISITOR_ID));
                binding.buttonPresence.setText(R.string.disable_presence);
            });
        } else if (event.getCode() == EventCode.EventPresenceDisconnected) {
            getActivity().runOnUiThread(() -> {
                binding.textviewFirst.setText(R.string.hello_first_fragment);
                binding.buttonPresence.setText(R.string.enable_presence);
                binding.buttonStart.setVisibility(View.VISIBLE);
                binding.buttonWebview.setVisibility(View.GONE);
                binding.buttonCustomTabs.setVisibility(View.GONE);

                anchorViews(binding.clFirstFragmentContainer, binding.buttonPresence, ConstraintSet.TOP,
                        binding.buttonStart, ConstraintSet.BOTTOM, R.dimen.default_presence_bt_margin_top);
            });
        }
    }

    protected void anchorViews(ConstraintLayout parentLayout, View view1, int direction1,
                               View view2, int direction2, int marginDimenId) {
        ConstraintSet anchorSet = new ConstraintSet();
        anchorSet.clone(parentLayout);

        anchorSet.connect(view1.getId(), direction1, view2.getId(), direction2,
                getResources().getDimensionPixelSize(marginDimenId));

        anchorSet.applyTo(parentLayout);
    }
}