package net.glance.twowayvideodemo_kotlin

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import net.glance.android.GlanceWebViewClient
import net.glance.android.GlanceWebViewJavascriptInterface
import net.glance.twowayvideodemo_kotlin.databinding.ActivityWebViewBinding

class WebViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebViewBinding
    var webView: WebView? = null
    var webViewClient: GlanceWebViewClient? = null
    var jsInterface: GlanceWebViewJavascriptInterface? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWebViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val url = intent.extras!!["url"] as String?
        val querySelectors = intent.extras!!["querySelectors"] as String?
        val labels = intent.extras!!["labels"] as String?

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.domStorageEnabled = true
        binding.webView.settings.mixedContentMode = 2
        jsInterface = GlanceWebViewJavascriptInterface(binding.webView)
        binding.webView.addJavascriptInterface(jsInterface!!, "GLANCE_Mask")

        webViewClient = GlanceWebViewClient(querySelectors, labels)
        binding.webView.webViewClient = webViewClient!!

        binding.webView.loadUrl(url!!)
    }

    override fun onDestroy() {
        webView?.removeJavascriptInterface("GLANCE_Mask")

        // you must call onDestroy on the javascript interface in order for the Glance SDK to properly clean up webView masks
        jsInterface?.onDestroy()
        jsInterface = null

        webViewClient = null

        super.onDestroy()
    }
}