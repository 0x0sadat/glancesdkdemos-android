package net.glance.twowayvideodemo_kotlin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import net.glance.android.HostSession
import net.glance.android.Settings
import net.glance.android.Visitor
import net.glance.android.VisitorInitParams
import net.glance.twowayvideodemo_kotlin.Constants.GLANCE_GROUP_ID
import net.glance.twowayvideodemo_kotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        val visitorInitParams = VisitorInitParams(GLANCE_GROUP_ID)
        Visitor.init(this, visitorInitParams)

        Log.d("GLANCE", "Version: " + Settings().Get("version"))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == HostSession.REQUEST_SCREEN_SHARE && Visitor.isInSession()) {
            if (resultCode == -1) {
                Visitor.onPermissionSuccess(data)
            } else if (resultCode == 0) {
                Visitor.onPermissionFailure()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}