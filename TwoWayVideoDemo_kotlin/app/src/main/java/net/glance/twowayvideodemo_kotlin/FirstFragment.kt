package net.glance.twowayvideodemo_kotlin

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import net.glance.android.*
import net.glance.twowayvideodemo_kotlin.Constants.GLANCE_GROUP_ID
import net.glance.twowayvideodemo_kotlin.Constants.VIDEO_MODE
import net.glance.twowayvideodemo_kotlin.Constants.VISITOR_ID
import net.glance.twowayvideodemo_kotlin.databinding.FragmentFirstBinding
import java.lang.String

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(), VisitorListener {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var inSession = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)

        Visitor.addMaskedView(binding.textviewSecond)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val self = this

        binding.buttonStart.setOnClickListener {
            Visitor.addListener(this)

            val startParams = StartParams()
            startParams.setKey("GLANCE_KEYTYPE_RANDOM")
            startParams.video = VIDEO_MODE
            Visitor.startSession(startParams)
        }

        binding.buttonPresence.setOnClickListener { v ->
            if (PresenceVisitor.isConnected()) {
                PresenceVisitor.disconnect()
            } else {
                val initParams = VisitorInitParams(GLANCE_GROUP_ID)
                initParams.visitorId = VISITOR_ID
                Visitor.init(activity, initParams)
                Visitor.addListener(self)
                PresenceVisitor.connect()
            }
        }

        binding.buttonCustomTabs.setOnClickListener { v ->
            val url = "https://google.com/"
            val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
                .setInitialActivityHeightPx(40)
                .setCloseButtonPosition(CustomTabsIntent.CLOSE_BUTTON_POSITION_END)

            val defaultColors: CustomTabColorSchemeParams = CustomTabColorSchemeParams.Builder()
                .setToolbarColor(self.resources.getColor(R.color.teal_700))
                .build()
            builder.setDefaultColorSchemeParams(defaultColors)

            val customTabsIntent: CustomTabsIntent = builder.build()
            customTabsIntent.launchUrl(self.requireContext(), Uri.parse(url))
        }

        binding.buttonWebview.setOnClickListener { v ->
            val i = Intent(context, WebViewActivity::class.java)
            i.putExtra("url", "https://d2e93a2oavc15x.cloudfront.net/")
            i.putExtra("querySelectors", ".mask_1, .mask_2, #mask_3, .mask_4, span, #hplogo")
            i.putExtra("labels", "mask 1, mask 2, mask 3, mask 4, span, LOGO")
            startActivity(i)
        }
    }

    override fun onDestroyView() {
        Visitor.removeListener(this)
        super.onDestroyView()
        _binding = null
    }

    override fun onGlanceVisitorEvent(event: Event?) {
        // TODO: move this event handler to MainActivity and pass variable into fragment to change button view states
        if (event != null) {
            if (event.code == EventCode.EventVisitorInitialized) {
                Log.d("GLANCE", "EventVisitorInitialized")
            } else if (event.code == EventCode.EventConnectedToSession) {
                activity?.runOnUiThread {
                    if (binding != null) {
                        binding.buttonStart.visibility = View.GONE
                        binding.buttonPresence.visibility = View.GONE
                        binding.buttonWebview.visibility = View.VISIBLE
                        binding.buttonCustomTabs.visibility = View.VISIBLE
                        if (PresenceVisitor.isConnected()) {
                            anchorViews(
                                binding.clFirstFragmentContainer,
                                binding.buttonPresence,
                                ConstraintSet.TOP,
                                binding.buttonCustomTabs,
                                ConstraintSet.BOTTOM,
                                R.dimen.enabled_presence_bt_margin_top
                            )
                        }
                    }
                }
            } else if (event.code == EventCode.EventSessionEnded) {
                activity?.runOnUiThread {
                    if (binding != null) {
                        binding.buttonStart.visibility = View.VISIBLE
                        binding.buttonPresence.visibility = View.VISIBLE
                        binding.buttonWebview.visibility = View.GONE
                        binding.buttonCustomTabs.visibility = View.GONE
                        if (PresenceVisitor.isConnected()) {
                            anchorViews(
                                binding.clFirstFragmentContainer,
                                binding.buttonPresence,
                                ConstraintSet.TOP,
                                binding.buttonStart,
                                ConstraintSet.BOTTOM,
                                R.dimen.default_presence_bt_margin_top
                            )
                        }
                    }
                }
            } else if (event.code == EventCode.EventPresenceConnected) {
                activity?.runOnUiThread {
                    binding.textviewFirst.text = String.format("Visitor id: %s", VISITOR_ID)
                    binding.buttonPresence.setText(R.string.disable_presence)
                }
            } else if (event.code == EventCode.EventPresenceDisconnected) {
                activity?.runOnUiThread {
                    binding.textviewFirst.setText(R.string.hello_first_fragment)
                    binding.buttonPresence.setText(R.string.enable_presence)
                    binding.buttonStart.visibility = View.VISIBLE
                    binding.buttonWebview.visibility = View.GONE
                    binding.buttonCustomTabs.visibility = View.GONE
                    anchorViews(
                        binding.clFirstFragmentContainer,
                        binding.buttonPresence,
                        ConstraintSet.TOP,
                        binding.buttonStart,
                        ConstraintSet.BOTTOM,
                        R.dimen.default_presence_bt_margin_top
                    )
                }
            }
        }
    }

    private fun anchorViews(
        parentLayout: ConstraintLayout?, view1: View, direction1: Int,
        view2: View, direction2: Int, marginDimenId: Int
    ) {
        val anchorSet = ConstraintSet()
        anchorSet.clone(parentLayout)
        anchorSet.connect(
            view1.id, direction1, view2.id, direction2,
            resources.getDimensionPixelSize(marginDimenId)
        )
        anchorSet.applyTo(parentLayout)
    }
}