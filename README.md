# GlanceSDKDemos-Android

Sample demo projects for Glance SDK on Android

This project contains the latest Android SDK and sample apps.
See the README file in each sample for quick-start instructions.

[Release Notes](https://gitlab.com/glance-networks/glancesdkdemos-ios/-/blob/master/ReleaseNotes.md)

For versions 4.8.x and earlier see:  
[GlanceSDKDemo_Android_Java](https://gitlab.com/glance-networks/GlanceSDKDemo_Android_Java)  
[GlanceSDKDemo_Android_Kotlin](https://gitlab.com/glance-networks/GlanceSDKDemo_Android_Kotlin)

